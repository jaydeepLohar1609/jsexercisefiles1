var fs = require('fs');


function fileSize (fileName, cb) {
    if (typeof fileName !== 'string') {
      return cb(new TypeError('argument should be string')); // Sync
    }
    fs.stat(fileName, (err, stats) => {
      if (err) { return cb(err); } // Async
      cb(null, stats.size); // Async
    });
  }

function callback1(arg1){
    // console.log(arg1)
}
fileSize('./staginsgApiHit.txt',callback1)
// fileSize(123,callback1);