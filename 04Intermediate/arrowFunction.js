let getClientDto = function (client) {
    return` Hey! ${client.name} welcome` 
}

let getClientDtoArrow =  (client) => {
    return ` Hey! ${client.name} welcome in arrow function`
}


let getClientDtoArrow_V2 =  (client) => ` Hey! ${client.name} welcome in arrow function`

console.log(getClientDtoArrow_V2({name : 'kartik Ji'}))

const Inters = [
    {
        name : 'vivek Ji',
        isFresher : false
    },
    {
        name : 'saurab Ji',
        isFresher : true
    },
    {
        name : 'Kartik Ji',
        isFresher : false
    },
    {
        name : 'Manoj Ji',
        isFresher : true
    }
]

// const experiencedEmployees = Inters.filter( (ele) => ele.isFresher === false)

const experiencedEmployees = Inters.filter( (ele) => ele.isFresher === false )

let getNameFromEmpObj = function (experiencedEmployees) {
    return experiencedEmployees.map( (ele) => ele.name)
}
console.log(`experienced inters are as ${getNameFromEmpObj(experiencedEmployees)}  `) 


