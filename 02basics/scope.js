let iAmGlobal = 'globalLet'

if (true) {
    // let iAmLocal = "localLet"
    var iAmLocal = "localLet" //var declare the variable in global scope
    iAmGlobal = 'updatedGlobalLet'
    console.log(iAmGlobal);
    console.log(iAmLocal);
}

console.log(iAmLocal);
console.log(iAmGlobal)

// Kings territory problem

// let king = 'king1'

if (true) {
    // let king = 'king2'
    // console.log("1st teritory ==============",king); 
    if (true) {
        // let king = 'king3'
        king = 'king3' // never ever declare a variable in these manner without defining its scope
        console.log("Sub 1st teritory==============",king); 
    }
}

if (true) {
    console.log("2nd teritory ==================",king);
}