// grade problem  10-5 Amazing   5-3 good   3-0 poor 0 fail

let studentMarks = [
    {marks : 9},
    {marks : 7},
    {marks : 4},
    {marks : 3},
    {marks : 2},
    {marks : 0},
    {marks : -1},
]

// studentMarks.map( x => {console.log("-------",typeof x.marks)})
studentMarks.map( x => {
        let marks = x.marks
        // console.log("-------------",marks,typeof marks)
        if ( 5 < marks && marks <= 10)  {
            console.log(marks,"-----------Amazing");
            // break;
        } else if(3 < marks && marks <= 5){
            console.log(marks,"------------work hard");
            // break;
        } else if (0 < marks && marks <= 3){
            console.log(marks,"------------- poor");
            // break;
        }else if(marks == 0){
            console.log(marks,"------------- fail");
            // break;
        }else{
            console.log(marks,"------------- Enter a valid input");
        }
})
