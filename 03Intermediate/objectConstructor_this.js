let youtubeVideoDetails = {
    videoId : 100,
    videoLength : 6.5,
    author: 'hitesh Choudhary',
    videoTopic : 'nodeJs Tutorials',
    youtubeVideoDetails : function (videoId, videoLength, author, videoTopic ) {
        this.videoId = videoId, 
        this.videoLength = videoLength, 
        this.author = author, 
        this.videoTopic = videoTopic
    },
    getYoutubeVideoDetails : function () {
        console.log("============",this)
        //refernce to type of youtubeVideoDetails
    },
    setYoutubeVideoDetails : function (params) {
        this.videoId = params.videoId,
        this.videoLength = params.videoLength,
        this.author = params.author,
        this.videoTopic = params.videoTopic
    }
}
youtubeVideoDetails.getYoutubeVideoDetails();

let employeeDetails = {
    employeeId : 100,
    employeeName : 'vidyut',
    dept: 'Tech',
    getemployeeDetails : function () {
        console.log(this)
        //refernce to type of employeeDetails
    },
    setemployeeDetails : function (params) {
        
    }
}
employeeDetails.getemployeeDetails();
// let youtubeVideoDetails1 = new youtubeVideoDetails(100,4,'hitesh','js tutorials');
// TypeError: youtubeVideoDetails is not a constructor
// bcoz we are not defining a class here so can't create object using new


youtubeVideoDetails.setYoutubeVideoDetails({
    videoId : 102,
    videoLength : 6.2,
    author: 'hitesh Choudhary',
    videoTopic : 'nodeJs Tutorials 2'
})
youtubeVideoDetails.getYoutubeVideoDetails();

// Int these case youtubeVideoDetails is mutable whenever we set any value it update the original object..
// So Working on the issue on v2 os same Topic