const days = ['mon', 'tue', 'wed', 'thr', 'fri', 'sat', 'sun']

console.log(days)
console.log(typeof days)

console.log(`Days in ascending order`)
for (let index = 0; index < days.length; index++) {
    const element = days[index];
    console.log(element)
    
}

console.log(`Days in descending order`)
for (let index = days.length - 1; index >= 0; index--) {
    const element = days[index];
    console.log(element)
    
}


console.log(`Displaying days array using for each loop in ascending order`);
days.forEach(element => {
    console.log(element)
});

console.log(`Displaying days array using for each loop in descending order`);
days.forEach((element,index) => {
    console.log(`index = ${index} and value = ${days[days.length - ( index + 1)]}` )
});

