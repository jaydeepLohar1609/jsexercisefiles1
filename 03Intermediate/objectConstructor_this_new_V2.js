
class GFG {
    constructor(A, B, C) {
        this.g = A;
        this.f = B;
        this.hh = C;
        this.getGFG = function () {
            console.log(this)
        }
        this.setGFG = function (params) {
            // this = params
            // ReferenceError: Invalid left-hand side in assignment this

            this.g = params.g,
            this.f = params.f,
            this.hh = params.hh
        }
    }
    print() { 
        console.log(this.g +"<br>"+this.f,"=====",this.hh); 
    } 
}
let gg = new GFG("JavaScript", "Java","Node"); 
gg.print(); 
console.log(gg)
// GFG { g: 'JavaScript', f: 'Java', hh: 'Node' }
// prints an Json Object but here it prints the class name also before object
console.log("===================================")
gg.getGFG()


gg.setGFG({
    g:'hi',
    f: 'hey',
    hh : 'hello'
})
// Value not update so only 
