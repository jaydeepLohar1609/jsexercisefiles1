let studentDetail = {
    name : 'krunal',
    age : 18,
    class : 'BSC'
}


let studentDetail2  = studentDetail;
studentDetail2.class = 'BCA'
console.log(studentDetail,studentDetail2)
// { name: 'krunal', age: 18, class: 'BCA' } { name: 'krunal', age: 18, class: 'BCA' }
// As simple assignment just pass a reference so it update original object's state.
// The copy variable is pointing to the same object and is a reference to that object.

console.log(`Naive Way of Copying Objects as below`)

function naiveCopy(originalObj) {
    let naiveCopyObj = {}
    for (const key in originalObj) {
        // if (originalObj.hasOwnProperty(key)) {
            // const naiveCopyObj[key] = originalObj[key];
            naiveCopyObj[key] = originalObj[key];

            
        // }
    }
    return naiveCopyObj
}

const mainObj = {
    a: 2,
    b: 5,
    c: {
      x: 7,
      y: 4,
    },
  }
  const duplicateObj = naiveCopy(mainObj)
  duplicateObj.b = 11
  duplicateObj.c.x = 10;
  console.log(mainObj);
  console.log(duplicateObj);
//   { a: 2, b: 5, c: { x: 10, y: 4 } }
// { a: 2, b: 11, c: { x: 10, y: 4 } }
// failes for the sub object as it is assigned a reference of anoterh object nt set value of key..


console.log(`Naive Way of Copying Objects as below`)




