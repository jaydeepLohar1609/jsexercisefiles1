
let getMyGrade = function (studentName, marksScored, totalMarks = 100) {
    // console.log(typeof marksScored)
    if (typeof marksScored !== "number") 
        return "Enter a number for 2 parameter for marksScored\n";
    let percent = ( marksScored / totalMarks ) * 100
    if (percent <= 100 && percent >= 90) {
        return `${studentName} obtained A grade and ${percent} percent\n` 
    } else if (percent <= 90 && percent >= 80){
        return `${studentName} obtained B grade and ${percent} percent\n` 
    } else if (percent <= 80 && percent >= 70) {
        return `${studentName} obtained C grade and ${percent} percent\n` 
    }else if (percent <= 70 && percent >= 60){
        return `${studentName} obtained D grade and ${percent} percent\n` 
    }else if (percent <= 60 && percent >= 50){
        return `${studentName} obtained E grade and ${percent} percent\n` 
    }else{
        return `${studentName} obtained F grade and ${percent} percent\n` 
    }
}


let studentDetails = [
    {name : 'kartik',marks : '65'},
    {name : 'krunal',marks : 95},
    {name : 'yuvraj',marks : 55},
    {name : 'sourabh',marks : 45}


]

studentDetails.forEach(element => {
    console.log(getMyGrade(element.name, element.marks));
});