superHeros = ['superMan', 'batMan', 'ironMan', 'spiderMan', 'hunk', 'captainAmerica', 'thor'];
console.log(superHeros)
console.log(superHeros.shift());
//returns first element by removing it from the array

console.log(superHeros);
console.log(superHeros.unshift('razorMan'));
//returns array length after inserting the given parameter into the first position of the array

console.log(superHeros.pop());
//returns the last element after removing it from the array

console.log(superHeros.push('avenger'))
//returns array length after inserting the given parameter into the last position of the array


console.log(superHeros)

console.log(superHeros.splice(1,2))
// return the array of elemntes removed from 1st index to count of 2 elements

console.log(superHeros.splice(1,1,'helloWorld'))
// return the array of elemntes removed from 1st index to count of 2 elements and add the 3rd parameter at 1st position

console.log(superHeros)


