//Synchronous execution
console.log(1)
console.log(2)
console.log(3)

//Async execution
console.log(4)
console.log(5)
setTimeout(() => {
    console.log(6,'||||||||||||||||||||||||||||||||||||||||||||||')
}, 1000);
console.log(7)

let blockingSyncFunctionConsoleNumber = function () {
    for (let index = 0; index < 500; index++) {
        process.stdout.write(index  + "-----Sync-----");
    }
}

let nonBlockingAsyncFunctionConsoleNumber = function () {
    setTimeout(() => {
        for (let index = 0; index < 500; index++) {
            process.stdout.write(index  + "----Async------");
        }
    },10000);    
}

nonBlockingAsyncFunctionConsoleNumber()

blockingSyncFunctionConsoleNumber()




// Non-blocking
const fs1 = require('fs');
fs1.readFile('/home/jaydeeplohar/Documents/movie2.mp4', (err1, data1) => {
  if (err1) throw err1;
  console.log(data1,'movie2');
});
// will run before console.log
console.log('+++++++++++++++++++++++after asynchronous readfile node function')



// Blocking
const fs = require('fs');
const data = fs.readFileSync('/home/jaydeeplohar/Documents/movie1.mp4'); // blocks here until file is read
console.log(data,'movie1');
// will run after console.log
console.log('++++++++++++++++++++++after synchronous readfile node function')



