

// JAI GANESHJI      JAI HANUMANJI      JAI GURUDEVJI :-))

 
function greeting(name) {
    alert('Hello ' + name);
  }
function processUserInput(callback) {
    // callback(name);
    var name = prompt('Please enter your name.');
    callback(name);
  }
processUserInput(greeting);



function first(){
    // Simulate a code delay
    //settimeout is a sync fuction
    setTimeout( function(){
      console.log(1);
    }, 0 );
  }
function second(){
    console.log(2);
}
first();
second();
  

function doHomework(subject, callback) {
    alert(`Starting my ${subject} homework.`);
    callback();
}  
doHomework('math', function() {
    alert('Finished my homework');
});


function doHomework(subject, callback) {
    alert(`Starting my ${subject} homework.`);
    callback();
}
function alertFinished(){
    alert('Finished my homework');
}
doHomework('math', alertFinished);
  

T.get('search/tweets', params, function(err, data, response) {
    if(!err){
      // This is where the magic will happen
    } else {
      console.log(err);
    }
})
  


function createQuote(quote, callback){ 
    var myQuote = "Like I always say, " + quote;
    callback(myQuote); // 2
}  
function logQuote(quote){
    console.log(quote);
}  
createQuote("eat your vegetables!", logQuote); // 1



function createQuote(quote, callback){ 
    var myQuote = "Like I always say, " + quote;
    callback(myQuote); // 2
}  
function logQuote(quote){
    console.log(quote);
}  
createQuote("eat your vegetables!", logQuote); // 1  
// Result in console: 
// Like I always say, eat your vegetables!



function serverRequest(query, callback){
    setTimeout(function(){
      var response = query + "full!";
      callback(response);
    },5000);
}
function getResults(results){
    console.log("Response from the server: " + results);
}  
serverRequest("The glass is half ", getResults);
// Result in console after 5 second delay:
// Response from the server: The glass is half full!