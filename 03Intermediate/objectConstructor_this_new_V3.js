
class employeeDto {
    constructor(id, name, dept) {
        this.id = id;
        this.name = name;
        this.dept = dept;
        this.getEmployeeDto = function () {
            console.log(this)
        }
        this.setEmployeeDto = function (employeeDetails) {
            // this = params
            // ReferenceError: Invalid left-hand side in assignment this

            this.id = employeeDetails.id,
            this.name = employeeDetails.name,
            this.dept = employeeDetails.dept
        }
    }
    print() { 
        console.log(this.id +"\n"+this.name,"\n",this.dept); 
    } 
}

let emp1 = new employeeDto(100, "Kapil Ji","Tech"); 
emp1.print(); 
console.log(emp1)
// GFG { g: 'JavaScript', f: 'Java', hh: 'Node' }
// prints an Json Object but here it prints the class name also before object

console.log("===================================")
emp1.getEmployeeDto()


emp1.setEmployeeDto({
   id : 101,
   name : "Kapil Ji",
   dept : "Tech"
})
// Value not update so only 
